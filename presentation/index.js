// Import React
import React from "react";

// Import Spectacle Core tags
import {
  Appear,
  BlockQuote,
  Cite,
  CodePane,
  Deck,
  Fill,
  Heading,
  Image,
  Layout,
  Link,
  ListItem,
  List,
  Markdown,
  Quote,
  Slide,
  Spectacle,
  Text
} from "spectacle";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Import custom component
import Interactive from "../assets/interactive";

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");


const images = {
  city: require("../assets/city.jpg"),
  kat: require("../assets/kat.png"),
  logo: require("../assets/formidable-logo.svg"),
  markdown: require("../assets/markdown.png"),
  gameScreenshot: require("../assets/game-screenshot.png"),
  functionalComponenets: require("../assets/functional-componenets.png"),
  reduxMain: require("../assets/redux-main.png"),
  redux1: require("../assets/redux-1.png"),
  redux2: require("../assets/redux-2.png"),
  redux3: require("../assets/redux-3.png"),
  redux4: require("../assets/redux-4.png"),
  reduxCode1: require("../assets/redux-code-1.png"),
  reduxCode2: require("../assets/redux-code-2.png"),
  reduxCode3: require("../assets/redux-code-3.png"),
  reduxCode4: require("../assets/redux-code-4.png"),
  componenetsDetailed: require("../assets/components-detailed.png"),
  dragSource: require("../assets/drag-source.png"),
  dropTarget: require("../assets/drop-target.png"),
};

preloader(images);

const theme = createTheme({
  primary: "#4787ed"
});

export default class Presentation extends React.Component {
  render() {
    return (
      <Spectacle theme={theme}>
        <Deck transition={["zoom", "slide"]} transitionDuration={500}>
          <Slide transition={["zoom"]} bgColor="primary">
            <Heading size={1} fit caps>
              A game of Ice and Fire
            </Heading>
            <Heading size={5} fit caps textColor="black">
              Game of thrones drag and drop game  
            </Heading>
            <Link href="https://github.com/nithinpeter/game-of-thrones">
              <Text textSize="1em" textColor="tertiary">View on Github</Text>
            </Link>
            <Text textSize="1em" margin="40px 0px 0px" bold>By</Text>
            <Text textSize="1.5em" margin="10px 0px 0px" bold>Nithin Peter</Text>
          </Slide>
          <Slide transition={["slide"]} bgColor="black">
            <Heading siz="3" fill textSize="4rem">
              <Link caps textColor="primary" textFont="primary" _target="blank" href="https://gameofthronesdnd.herokuapp.com">
                Demo!
              </Link>
            </Heading> 
            <Image src={images.gameScreenshot.replace("/", "")} height="70vh"/>
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading siz="1" fill textSize="4rem">Technology Stack</Heading>
            <List>
              <ListItem>React</ListItem>
              <ListItem>Redux</ListItem>
              <ListItem>React DnD</ListItem>
              <ListItem>Enzyme</ListItem>
            </List>
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading siz="1" fill textSize="4rem">React</Heading>
            <List>
              <ListItem>Declarative</ListItem>
              <ListItem>Composable Components</ListItem>
              
                <List style={{paddingLeft: 50}}>
                  <Appear><ListItem>Presentational Components</ListItem></Appear>
                  <Appear><ListItem>Container Components</ListItem></Appear>
                </List>
            </List>
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading siz="1" fill textSize="4rem">Presentational Components</Heading>
            <List>
              <ListItem>Are concerned with how things look</ListItem>
              <ListItem>Don't specify how data is loaded</ListItem>
              <ListItem>Implemented as pure, stateless functional compnents</ListItem>
              <Image
                src={images.functionalComponenets}
                margin="20px 50px" />
            </List>
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading siz="1" fill textSize="4rem">Container Components</Heading>
            <List>
              <ListItem>Are concerned with how things work</ListItem>
              <ListItem>Provide the data and behavior to presentational or other container components</ListItem>
            </List>
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading size={1} fill textSize="4rem">Redux</Heading>
            <Image
                src={images.componenetsDetailed}
                height="70vh" />
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading size="1" fill textSize="4rem">Redux</Heading>
            <Image
                src={images.reduxMain}
                margin="20px 50px" />
            <Text textSize="1.5em" margin="10px 0px 0px" textColor="white">State management library for javascript apps.</Text>
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Text textSize="1.5em" margin="10px 0px 0px" textColor="white">Do Something!</Text>
            <Image
                src={images.redux1}
                margin="20px 50px" />
            <Image
                src={images.reduxCode1}
                margin="20px 50px" />
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Text textSize="1.5em" margin="10px 0px 0px" textColor="white">Translate</Text>
            <Image
                src={images.redux2}
                margin="20px 50px" />
            <Image
                src={images.reduxCode2}
                margin="20px 50px" />
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Text textSize="1.5em" margin="10px 0px 0px" textColor="white">Store</Text>
            <Image
                src={images.redux3}
                margin="20px 50px" />
            <Image
                src={images.reduxCode3}
                margin="20px 50px" />
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Text textSize="1.5em" margin="10px 0px 0px" textColor="white">Render</Text>
            <Image
                src={images.redux4}
                margin="20px 50px" />
            <Image
                src={images.functionalComponenets}
                margin="20px 50px" />
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading size="1" fill textSize="4rem">Redux In Action</Heading>
          </Slide>
          
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading siz="1" fill textSize="4rem">React DnD</Heading>
            <List>
              <ListItem>Set of higher order components to build drag and drop interfaces</ListItem>
              <List style={{marginLeft: 50}}>
                <ListItem>DragSource</ListItem>
                <Image
                  src={images.dragSource}
                  margin="20px 50px" />

                <ListItem>DropTarget</ListItem>
                <Image
                  src={images.dropTarget}
                  margin="20px 50px" />
              </List>
            </List>    
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading siz="1" fill textSize="4rem">Enzyme</Heading>
            
          </Slide>
          
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading siz="1" fill textSize="4rem">Technology stack - other</Heading>
            <List>
              <ListItem>React-redux : React bindings for Redux</ListItem>
              <ListItem>Mocha : Test runner</ListItem>
              <ListItem>Chai-enzyme : Chai.js assertions for enzyme</ListItem>
              <ListItem>Webpack : Build tool</ListItem>
              <ListItem>Babel : ES6 and JSX transpiler</ListItem>
              <ListItem>Firebase</ListItem>
            </List>
          </Slide>
          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Heading siz="1" fill textSize="4rem">How can I make it a multi-player game?</Heading>
            <List>
              <ListItem>React-redux : React bindings for Redux</ListItem>
              <ListItem>Mocha : Test runner</ListItem>
              <ListItem>Chai-enzyme : Chai.js assertions for enzyme</ListItem>
              <ListItem>Webpack : Build tool</ListItem>
              <ListItem>Babel : ES6 and JSX transpiler</ListItem>
              <ListItem>Firebase</ListItem>
            </List>
          </Slide>
        </Deck>
      </Spectacle>
    );
  }
}
